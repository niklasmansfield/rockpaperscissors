import random

# coding: utf-8
#
# STEN, SAX OCH PÅSE
# ==================


# SLUMP
# =====
# För att slumpa värden kan man använda funktioner ur modulen random. Titta i
# dokementationen för denna modul.
#
# http://docs.python.org/3.3/library/random.html#module-random
#
# Bra alternativ för att lösa denna uppgift kan vara funktionerna randint eller
# sample.
#
# http://docs.python.org/3.3/library/random.html#random.randint
# http://docs.python.org/3.3/library/random.html#random.sample
def ai_move(moves):
    """Returns a random move from moves.

    Arguments:
            moves: a tuple of strings where each element is a move.

    Returns:
            one of the moves, chosen randomly.
    """
    return random.sample(moves, 1)[0]
    


# Funktionen avgör om ett drag är giltigt. Ett drag är giltigt om det finns i
# tupeln moves.
def is_valid(move, moves):
    """Returns True if move is valid, False Otherwise.

    Arguments:
            move: a string.
            moves: a tuple of strings where each element is a move.

    Returns:
        True of False.
    """
    return move in moves


# Denna funktion ska avgöra om en dator fuskar. Chansen för detta är argumentet
# chance, i procent.
def cheat(chance):
    """Determines if the ai cheats.

    Arguments:
            chance: the chance, in percent, of the ai cheating.

    Returns:
            True or False.
    """
    return random.randint(1,100) <= chance


def evaluate(pl_move, ai_move, chance = 0):
    """Determines wether player wins, ai wins or there is a tie.

    Arguments:
        pl_move: the palyer's move, a string.
        ai_move: the ai's move.
        chance: the chance of the ai cheating, in percent.

    Returns:
        1  if the player wins.
        0  if there is a tie
       -1  if the ai wins
    """
    
    if cheat(chance):
        return -1
    
    moves = ('rock', 'paper', 'scissors')
    if pl_move == ai_move:
        return 0
    elif (moves.index(pl_move) + 1) % 3 == moves.index(ai_move):
         return -1
    else:
        return 1


def feedback(pl_move, ai_move, win):
    """Returns a status message with results to be printed."""


def main():
    """Entry point for the program."""
    # clear screen
    print("\x1b[2J\x1b[1;1H", end="")
    
    print("Let's, play some rock, paper, scissors")
    
    moves = ('rock', 'paper', 'scissors')
    print(str(type(moves)))
    
    while True:
        pl_move = input('Make your move (rock, paper or scissors): ').lower()
        
        while not is_valid(pl_move, moves):
            pl_move = input("Not a valid choice. Write 'paper', 'rock' or 'scissors' (without the '). Try again: ")
        
        comp_move = ai_move(moves)

        print('Player: ' + pl_move + '. AI: ' + comp_move)
    
        if evaluate(pl_move, comp_move) == -1:
            print('Computer wins!')
        elif evaluate(pl_move, comp_move) == 0:
            print("It's a draw.")
        else:
            print('You win! Well played.')
    

if __name__ == '__main__':
    main()
